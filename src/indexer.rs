//! The indexer is parsing the input and creates an index that can be re-used.
//! This will be useful for big files.
use std::path::PathBuf;

use crate::{
    cli_options::IndexingOptions,
    score::{ScoringAlgo},
    PresentationOptions, serialized_content::{LineNumber, SerializedContent, LineScores}, file_manager::FileManager,
};

trait Indexer {
    fn create_index();
}

/// Structure of our index.
#[derive(Debug, Clone)]
pub struct Index<'a> {
    /// Reference to the content
    content: &'a str,

    /// Vec of the lines
    lines: Vec<&'a str>,

    /// Content of the index
    index: SerializedContent, 
}

impl Index<'_> {
    pub fn new(algo: ScoringAlgo, content: &str, path: Option<PathBuf>) -> Index {
        Index {
            content,
            lines: content.split("\n").collect(),
            index: SerializedContent::new(algo, content, path),
        }
    }

    pub fn create_index(&mut self, opts: &IndexingOptions) {
        for i in 0..self.lines.len() {
            self.process_line( i, opts);
        }
    }

    pub fn normalize(mut str: String, opts: &IndexingOptions) -> String{
        if !opts.case_sensitive {
            str = str.to_lowercase();
        }
        
        if opts.ignore_spaces {
            str = str.replace(" ", "");
        }

        str
    }

    pub fn process_line(&mut self, n: LineNumber, opts: &IndexingOptions) {    
        // TODO: we could optimize by not calling normalize at all if not required
        let base = Index::normalize(String::from(self.lines[n]), opts);

        for i in (n + 1)..=(n + opts.scope).min(self.lines.len() - 1) {
            let normalized_content = Index::normalize(String::from(self.lines[i]), opts);
            let score = self.index.calculator.get_score(&base, &normalized_content);
            let m = self.index.index.entry(n).or_insert(LineScores::new());
            m.insert(i, score);
        }
    }

    pub fn get_index_content(&self) -> &SerializedContent {
        &self.index
    }

    /// Return the number of matches for a given line
    pub fn get_match_count(&self, n: &LineNumber, opts: &PresentationOptions) -> usize {
        match self.get_matches(n, opts) {
            None => 0,
            Some(s) => s.len()
        }
    }

    pub fn get_matches (&self, n: &LineNumber, opts: &PresentationOptions) -> Option<Vec<LineNumber>> {
        let mut res: Vec<LineNumber> = Vec::new();
        match self.index.index.get(n) {
            None => None,
            Some(line_scores) => {
                line_scores.iter()
                    .filter(|item| item.1 <= &opts.threshold)
                    .for_each( |x| res.push( (*x.0).clone()) );
                Some(res)
            },
        }
    }

    pub fn get_lines(&self) -> &Vec<&str> {
        &self.lines
    }

    pub fn get_hash(&self) -> &str {
        &self.index.hash
    }

    pub fn save(&self) -> std::io::Result<PathBuf> {
        let folder = self.index.path.clone();
        let target = FileManager::new(folder).get_file(self.get_hash());
        match self.index.save(&target) {
            Ok(_) => Ok(target),
            Err(e) => Err(e)
        }
    }

    pub fn load(&mut self) -> Option<()> {
        match SerializedContent::load(self.get_hash()) {
            Ok(content) => {
                self.index = content;
                Some(())
            }
            _ => None
        }
    }

    /// This function tried loading the index from file.
    /// If that fails for any reason, a new index will be created.
    pub fn load_or_create_index(&mut self, opts: &IndexingOptions) {
        let load_result = self.load();
    
        // println!("Index Loading result: {:?}", load_result);
        if load_result.is_none() {
            self.create_index(&opts)
        } 
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::iter::FromIterator;
    
    #[test]
    fn test_full() {
        let data = String::from(vec!["Line1", "Line2", "LIne3", "LINe4", "MEGA JUNK"].join("\n"));
        let mut index = Index::new(ScoringAlgo::Levenshtein, &data, Some(PathBuf::from("sample/path")));
        index.create_index(&IndexingOptions::default());

        println!("{:#?}", index.get_index_content());
    }

    #[test]
    fn test_matches() {
        let data = String::from(vec!["Line1", "Line2", "LIne3", "LINe4", "MEGA JUNK"].join("\n"));
        let mut index = Index::new(ScoringAlgo::Levenshtein, &data, Some(PathBuf::from("sample/path")));
        let index_options = IndexingOptions {
            scope: 10,
            ..IndexingOptions::default()
        };
        index.create_index( &index_options);
        
        let mut matches = index.get_matches(&0, &PresentationOptions::default()).unwrap();
        matches.sort();

        assert_eq!(matches, vec![1,2,3]);
        assert_eq!(index.get_match_count(&0, &PresentationOptions::default()), 3);
    }

    #[test]
    fn test_long_lines() {
        let s1 = String::from_iter(vec!['a'; 1024]);
        let s2 = String::from_iter(vec!['z'; 1024]);
        let data = String::from(vec![s1, s2].join("\n"));

        let mut index = Index::new(ScoringAlgo::Levenshtein, &data, Some(PathBuf::from("sample/path")));
        let index_options = IndexingOptions {
            scope: 10,
            ..IndexingOptions::default()
        };
        index.create_index( &index_options);
    }

    #[test]
    #[ignore] // This test fails from time to time. It seems that we read the file before it is completely written.
    fn test_save_load() {
        let data = String::from(vec!["Line1", "Line2", "LIne3", "LINe4"].join("\n"));
        let mut index = Index::new(ScoringAlgo::Levenshtein, &data, Some(PathBuf::from("junk")) );
        index.create_index(&IndexingOptions::default());

        match index.save() {
            Ok(_) => println!("Index saved"),
            Err(e) => println!("{:?}", e),
        }
        
        let mut new_index = Index::new(ScoringAlgo::Levenshtein, &data, None );
        assert!(new_index.load().is_some());
    }

    #[test]
    fn test_load_unavailable() {
        let data = String::from(vec!["Line1", "Line2"].join("\n"));
        let mut index = Index::new(ScoringAlgo::Levenshtein, &data, Some(PathBuf::from("junk")) );
        
        assert!(index.load().is_none());
    }
}
