use serde::{Serialize, Deserialize};
use blake2::VarBlake2b;
use blake2::digest::{Update, VariableOutput};
use std::{path::PathBuf, collections::HashMap, fs::File, io::{Write, Read}};
use crate::{file_manager::FileManager, score::{ScoringAlgo, ScoreCalculator, Score}};

pub type LineNumber = usize;
pub type LineScores = HashMap<LineNumber, Score>;
pub type IndexContent = HashMap<LineNumber, LineScores>;

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct SerializedContent {
    /// Path of the file that is being indexed
    /// This will be None if the input is stdin
    pub path: Option<PathBuf>,
        
    pub index: IndexContent,

    /// Distance calculator
    pub calculator: ScoreCalculator,
    
    pub hash: String,
}

impl SerializedContent {
    pub fn new (algo: ScoringAlgo, content: &str, path: Option<PathBuf>) -> SerializedContent {
        SerializedContent {
            path,
            index: IndexContent::new(),
            calculator: ScoreCalculator::new(algo),
            hash: Self::get_content_hash(content),
        }
    }

    pub fn get_content_hash(content: &str) -> String {
        let mut hasher = VarBlake2b::new(16).unwrap();
        hasher.update(&content);
        let mut hash = String::new();
        hasher.finalize_variable(|res| {
            hash = hex::encode(res);
        });
        hash
    }

    pub fn save(&self, path: &PathBuf) -> std::io::Result<()> {
        let serialized = serde_json::to_vec(&self)?;
        let mut file = File::create(path)?;
        file.write_all(&serialized)?;
        file.flush()
    }

    pub fn load(hash: &str) -> std::io::Result<SerializedContent> {
        let fm = FileManager::new(None);
        let path = fm.get_file(hash);
        let mut file = File::open(path)?;
        let mut content: Vec<u8> = Vec::<u8>::new();
        file.read_to_end(&mut content)?;

        let index= serde_json::from_slice(&content).expect("Failed loading index");
        Ok(index)
    }

    pub fn get_hash(&self) -> &str {
        &self.hash
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{file_manager::FileManager};

    #[test]
    fn test_save_load() {
        let data = String::from(vec!["Line1", "Line2", "LIne3", "LINe4", "MEGA JUNK"].join("\n"));
        let index = SerializedContent::new(ScoringAlgo::Levenshtein, &data, Some(PathBuf::from("sample/path")));
        
        let hash = index.get_hash();
        let path = FileManager::new(None).get_file(hash);
        match index.save(&path) {
            Ok(_) => println!("Index saved at {:?}", path.to_str()),
            Err(e) => println!("{:?}", e),
        }

        let new_index = SerializedContent::load(hash).expect("Failed loading index");
        println!("{:?}", new_index);
    }
}