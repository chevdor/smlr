use eddie::DamerauLevenshtein;
use eddie::Levenshtein;
use std::fmt;

pub type Score = u8;
use serde::{Serialize, Deserialize};

pub trait Scoring<T> {
    fn get_score(_: &T, _: &T) -> Score;
}

#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub enum ScoringAlgo {
    Levenshtein,
    DamerauLevenshtein,
}

impl From<&str> for ScoringAlgo {
    // TODO: It would be nice to NOT have to repeat the enum content below
    fn from(s: &str) -> Self {
        match s {
            "Levenshtein" => ScoringAlgo::Levenshtein,
            "DamerauLevenshtein" => ScoringAlgo::DamerauLevenshtein,
            _ => panic!("Algo '{}' not supported", s),
        }
    }
}

impl ScoringAlgo {
    // TODO: It would be nice to NOT have to repeat the enum content below
    pub fn iterator() -> impl Iterator<Item = ScoringAlgo> {
        [ScoringAlgo::Levenshtein, ScoringAlgo::DamerauLevenshtein].iter().copied()
    }

    pub fn get_choices() -> Vec<String> {
        ScoringAlgo::iterator().fold(Vec::new(), |mut acc, algo| {
            acc.push(algo.to_string());
            acc
        } )
    }
}

impl fmt::Display for ScoringAlgo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ScoreCalculator {
    scoring_algo: ScoringAlgo,
}

fn capped_downcast (big: usize) -> Score
{
    match big > Score::MAX.into() {
        true => Score::MAX,
        false => big as Score,
    }
} 

impl ScoreCalculator {
    pub fn new(scoring_algo: ScoringAlgo) -> ScoreCalculator {
        ScoreCalculator { scoring_algo }
    }

    pub fn get_score(&self, s1: &str, s2: &str) -> Score {
        let distance = match self.scoring_algo {
            ScoringAlgo::Levenshtein => 
                Levenshtein::new().distance(s1, s2),
            ScoringAlgo::DamerauLevenshtein => 
                DamerauLevenshtein::new().distance(s1, s2)
        };
        capped_downcast(distance)
    }
}

#[cfg(test)]
mod tests {
    extern crate eddie;
    extern crate test;

    use super::*;
    use test::Bencher;
    use std::iter::FromIterator;

    #[test]
    fn test_get_score_lev() {
        let calc = ScoreCalculator::new(ScoringAlgo::Levenshtein);
        let s1 = &String::from("kitten");
        let s2 = &String::from("sitting");
        assert_eq!(calc.get_score(s1, s2), 3);
    }

    #[test]
    fn test_score_long_lines() {
        let calc = ScoreCalculator::new(ScoringAlgo::Levenshtein);
        let length: usize = 2usize.pow(10); // 10 bits no longer fits our u8
        let s1 = &String::from_iter(vec!['a'; length]);
        let s2 = &String::from_iter(vec!['z'; length]);
        assert_eq!(calc.get_score(s1, s2), Score::MAX);
    }
    
    #[test]
    fn test_get_score_damlev() {
        let calc = ScoreCalculator::new(ScoringAlgo::DamerauLevenshtein);
        let s1 = &String::from("kitten");
        let s2 = &String::from("kottne");
        assert_eq!(calc.get_score(s1, s2), 2);
    }

    #[test]
    fn test_get_score_str() {
        let calc = ScoreCalculator::new(ScoringAlgo::Levenshtein);
        assert_eq!(calc.get_score(&"kitten", &"sitting"), 3);
    }

    #[bench]
    fn bench_eddie_levenshtein(b: &mut Bencher) {
        let lev = Levenshtein::new();
        b.iter(|| lev.distance("kitten", "sitting"));
    }

    #[test]
    fn test_str_to_algo_enum() {
        let algo = ScoringAlgo::from("Levenshtein");
        assert_eq!(algo, ScoringAlgo::Levenshtein);
    }

    #[test]
    #[should_panic(expected= "Algo 'junk' not supported")]
    fn test_str_to_algo_enu_bad() {
        ScoringAlgo::from("junk");
    }

    #[test]
    fn test_get_list_of_algos() {
        let lst = ScoringAlgo::get_choices();
        println!("{:?}", lst);
    }
}
