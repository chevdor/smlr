#![feature(test)]

pub mod cli_options;
pub mod indexer;
pub mod score;
pub mod file_manager;
pub mod serialized_content;

pub use cli_options::{CliOptions, PresentationOptions, IndexingOptions};
use indexer::{Index};
use std::{path::PathBuf, io, result::Result};
use serialized_content::LineNumber;
use io::Read;

/// Show the headers depending on the [CliOptions].
fn show_headers_maybe(opts: &CliOptions) {
    // Show headers if requested
    if opts.presentation.headers {
        if opts.presentation.line_numbers {
            print!("Line#\t");
        }

        if opts.presentation.count {
            print!("Count\t");
        }

        println!("Input");
        println!("----------------------");
    }
}

/// Display (or not) a single line depending on the [matches](Index::get_matches) from the [Index].
fn show_line(lines: &Vec<&str>, i: LineNumber, matches: &Option<Vec<LineNumber>>, opts: &CliOptions) {
    let matches_count = match matches {
        None => 0,
        Some(vec) => vec.len(),
    };

    if opts.presentation.line_numbers {
        print!("{:05}\t", i);
    }

    if opts.presentation.count {
        print!("{}\t", matches_count + 1); // without +1 we get only the number of dups without the ref item
    }

    println!("{}", lines[i]);
}

/// After the [Index] was created, we can use it to filter and display the filtered input.
pub fn display(lines: &Vec<&str>, index: &Index, opts: &CliOptions) {
    let seq: Vec<usize> = (0usize..lines.len()).collect();
    let mut used = Vec::<usize>::with_capacity(lines.len());
    
    show_headers_maybe(opts);

    for i in seq {
        let matches = index.get_matches(&i, &opts.presentation);
        match matches {
            None => {},
            Some(ref m) => {
                m.iter()
                .for_each(|line_ref| {
                    used.push(*line_ref);   
                });
            }
        }

        if !used.contains(&i) && !opts.presentation.inverted {
            show_line(lines, i, &matches, opts);
        } else {
             if used.contains(&i) &&  opts.presentation.inverted {
                 show_line(lines, i, &matches, opts);
            }
        }
    }
}

/// This function reads the file provided as [filename] of get the input
/// from stdin if [flename] was None.
pub fn get_input(filename: &Option<PathBuf> ) -> Result<String, String> {
    match filename {
        Some(path) => {
            match std::fs::read_to_string(path) {
                Ok(s) => Ok(s),
                Err(_) => Err(String::from("Error reading file"))
            }
        }
        None => {
            println!("Waiting for data on stdin. CTR+D once done.");
            let stdin = io::stdin();
            let mut stdin = stdin.lock();
            
            let mut buf: Vec<u8> = Vec::new();
            if stdin.read_to_end(&mut buf).is_err() {
                return Err(String::from("sf"));
            }
            
            if buf.len() == 0 || buf == &[10] {
                println!("\n🤚 If you pass data using stdin, it must not be empty!");
                return Err(String::from("\n🤚 If you pass data using stdin, it must not be empty!"));
            }
            
            Ok(String::from( std::str::from_utf8(&buf).unwrap()))
        }
    }
}

pub fn run(input: &str, filename: Option<PathBuf>, options: &CliOptions) {
    let mut index = Index::new(options.indexing.algo, &input, filename);

    index.load_or_create_index( &options.indexing);    
    
    // TODO: We should only save if we did not load
    match options.persistence.persist
    {
        true => match index.save() {
            Ok(target) => if options.presentation.verbose > 0 { println!("Index saved successfully at {:?}", target) },
            Err(e) => println!("Error loading index: {:?}", e),
        },
        false => if options.presentation.verbose > 0 { println!("Skipped saving index") }
    } 

    display(&index.get_lines(), &index, &options);
}

#[cfg(test)]
mod tests {
    use super::*;
    use cli_options::IndexingOptions;
    use score::ScoringAlgo;

    #[test]
    fn test_not_inverted() {
        let opts = CliOptions {
            indexing: IndexingOptions {
                scope: 10, 
                algo: ScoringAlgo::Levenshtein,
                ..IndexingOptions::default()
            },
            
            presentation: PresentationOptions {
                threshold: 1,
                line_numbers: true,
                count: true,
                inverted: false,
                headers: true,
                ..PresentationOptions::default()
            },

            ..CliOptions::default()
        };

        let content = vec![
            "Line1",        // #0 OK
            "Lin1e",        // #0 OK
            "XXX1",         // #1 OK
            "Line2",        // #2 DUP #1
            "LIne3",        // #3 DUP #1
            "LINe4",        // #4 DUP #1
            "XXX2",         // #5 DUP #2
            "Mega Junk",    // #6 OK
        ]
        .join("\n");
        run(&content,Some(PathBuf::from("sample/path")), &opts)
    }

    #[test]
    fn test_inverted() {
        let opts = CliOptions {
            indexing: IndexingOptions {
                scope: 10, 
                ..IndexingOptions::default()
            },
            
            presentation: PresentationOptions {
                threshold: 3,
                line_numbers: true,
                count: false,
                inverted: true,
                headers: true,
                ..PresentationOptions::default()
            },

            ..CliOptions::default()
        };

        let content = vec![
            "Line1",        // #0 OK
            "XXX1",         // #1 OK
            "Line2",        // #2 DUP #1
            "LIne3",        // #3 DUP #1
            "LINe4",        // #4 DUP #1
            "XXX2",         // #5 DUP #2
            "Mega Junk",    // #6 OK
        ]
        .join("\n");
        run(&content,Some(PathBuf::from("sample/path")),  &opts)
    }
}
