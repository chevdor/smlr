//! Struct to hold the various options. Those are fed by the user through [clap].

use crate::score::*;
use clap::{value_t, ArgMatches};
use std::path::PathBuf;

/// Options related to the presentation and filtering.
/// See also [CliOptions].
#[derive(Debug)]
pub struct PresentationOptions {
    pub debug: bool,
    pub count: bool,
    pub line_numbers: bool,
    pub inverted: bool,       
    pub threshold: Score,   // TODO: probably should be renamed into distance
    pub headers: bool,      

    /// Whether we show a progress bar
    pub progrsess: bool,  
    pub verbose: u64,    
}

/// Options related to the indexing.
/// See also [CliOptions].
#[derive(Debug)]
pub struct IndexingOptions {
    pub case_sensitive: bool,
    pub ignore_spaces: bool,
    pub scope: usize,
    pub full: bool,
    pub algo: ScoringAlgo,
}

#[derive(Debug)]
pub struct PersistenceOptions {
    /// whether we persist the index or not
    pub persist: bool,

    /// Index storage location. If None, it will be put 'next' to the input file.
    /// If None and using stin, it will be stored in the currenf folder.
    /// If Some(path), no matter whether the input is a file or stdin, the index
    /// files will be stored at "path".  
    pub location: Option<PathBuf>,
}

/// Options for the CLI. The options are split into categories.
/// See [IndexingOptions] and [PresentationOptions].
#[derive(Debug)]
pub struct CliOptions {
    pub indexing: IndexingOptions,
    pub presentation: PresentationOptions,
    pub persistence: PersistenceOptions,
}

impl CliOptions {
    pub fn new(matches: &ArgMatches, buffer_length: usize) -> CliOptions {
        let scope = if matches.is_present("full") {
            buffer_length
        } else {
            value_t!(matches.value_of("scope"), usize).unwrap_or(3)
        };
        
        let algo_name = matches.value_of("algo").unwrap();
        let algo =  ScoringAlgo::from( algo_name);

        CliOptions {
            indexing: IndexingOptions {
                case_sensitive: matches.is_present("case-sensitive"),
                ignore_spaces: matches.is_present("ignore-spaces"),
                scope,
                full: matches.is_present("full"),
                algo,

                ..Default::default()
            },
            presentation: PresentationOptions {
                inverted: matches.is_present("invert"),
                threshold: if matches.is_present("strict") { 0 } else { value_t!(matches.value_of("distance"), u8).unwrap_or(3)},
                line_numbers: matches.is_present("line-numbers"),
                count: matches.is_present("count") && !matches.is_present("invert"),
                debug: matches.is_present("debug"),
                headers: matches.is_present("headers"),
                verbose: matches.occurrences_of("verbose"),

                ..Default::default()
            },

            persistence: PersistenceOptions {
                persist: !matches.is_present("no-index"),
                
                ..Default::default()
            },

            ..Default::default()
        }
    }
}

impl Default for CliOptions {
    fn default() -> Self {
        CliOptions {
            indexing: IndexingOptions::default(),
            presentation: PresentationOptions::default(),
            persistence: PersistenceOptions::default(),
        }
    }
}

impl Default for PresentationOptions {
    fn default() -> Self {
        PresentationOptions {
            inverted: false,
            threshold: 3,
            line_numbers: false,
            count: false,
            debug: false,
            headers: true,
            progrsess: false,
            verbose: 0,
        }
    }
}

impl Default for IndexingOptions {
    fn default() -> Self {
        IndexingOptions {
            case_sensitive: false,
            ignore_spaces: false,
            scope: 10,
            full: false,
            algo: ScoringAlgo::Levenshtein,
        }
    }
}

impl Default for PersistenceOptions {
    fn default() -> Self {
        PersistenceOptions{
            persist: false,
            location: None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_default() {
        let default_options: CliOptions = Default::default();
        assert_eq!(default_options.indexing.scope, 10);
        assert_eq!(default_options.presentation.inverted, false);
        assert_eq!(default_options.presentation.threshold, 3);
    }
}
