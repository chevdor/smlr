
//! No longer used, will likely be removed.

use crate::score::Scoring;
use crate::{normalize::Normalize, score::Score};
use eddie::Levenshtein;
use std::convert::TryInto;
use std::fmt;

#[derive(Debug)]
pub struct Line {
    /// one line
    pub input: String,

    /// The line after applying all cleanups and normalization
    pub processed: String,

    /// Line number
    pub number: Option<usize>,

    pub matches: Vec<Score>,
}

impl Line {
    pub fn new(line: String) -> Line {
        Line {
            input: line.clone(),
            processed: line,
            number: None,
            matches: vec![],
        }
    }
}

impl Normalize for Line {
    fn spaces(&mut self) {
        self.processed = String::from(self.processed.replace(" ", ""))
    }

    fn case(&mut self) {
        self.processed = String::from(self.processed.to_lowercase())
    }
}

impl fmt::Display for Line {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(
            f,
            "{}\t| {} | {:?}",
            self.input, self.processed, self.matches
        )
    }
}

impl Scoring<Line> for Line {
    /// A Line can be a simple String or a Vec<String>
    /// For a Vec<String>, the score is the sum of the individual scores
    fn get_score(one: &Line, other: &Line) -> Score {
        let lev = Levenshtein::new();
        lev.distance(&one.processed, &other.processed)
            .try_into()
            .expect("Out of bound conversion to score")
    }
}

#[cfg(test)]
mod tests {
    use super::Line;
    use crate::score::Scoring;

    #[test]
    fn test_line_1() {
        let s1 = String::from("kitten");
        let s2 = String::from("sitting");
        let l1 = Line::new(s1);
        let l2 = Line::new(s2);
        assert_eq!(Line::get_score(&l1, &l2), 3);
    }
}
