use std::{env, path::PathBuf};

/// This struct helps us manage the location of the persistence files depending on the [CliOptions]
/// provided by the user or the default. See especially the [PersistenceOptions].
#[derive(Debug)]
pub struct FileManager {
    pub base_path: PathBuf
}

impl FileManager {
    /// The input path may be absolution or relative
    pub fn new(path: Option<PathBuf>) -> FileManager {
        FileManager {
            base_path: match path {
                None => env::temp_dir(),
                Some(_p) => env::temp_dir(), // TODO: for now, all in /tmp but here we should get an absolute path
            } 
        }
    }

    /// Given a content hash, this function returns the full path of the index
    pub fn get_file(&self, hash: &str) -> PathBuf {
        let mut res = self.base_path.clone();
        res.push(format!("{}.smlr", hash));
        res
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::ffi::{OsString};

    #[test]
    fn test_new() {
        let fm = FileManager::new(None);
        assert_eq!(fm.base_path, PathBuf::from(env::temp_dir()));
    }

    #[test]
    fn test_from_hash() {
        let fm = FileManager::new(None);
        let file = fm.get_file("foobar");
        assert_eq!(file.parent().unwrap(), PathBuf::from(env::temp_dir()));
        assert_eq!(file.file_name().unwrap(), OsString::from("foobar.smlr"));
    }

    #[test]
    #[ignore]
    fn test_new_from_path() {
        let fm = FileManager::new(Some(PathBuf::from("/tmp/a/b/c")));
        println!("{:?}", fm);
        assert_eq!(fm.base_path, PathBuf::from("/tmp/a/b/c"));
    }
}
              