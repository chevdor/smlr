//! This CLI allows finding similar lines in a file / stdin.

extern crate clap;

use clap::{crate_version, App, Arg};
use std::{io, path::PathBuf};
use smlr::{CliOptions, run, get_input};

fn main() -> io::Result<()> {
    let app = App::new("smlr");

    let default_opts = CliOptions::default();
    let default_scope: &str = &default_opts.indexing.scope.to_string();

    let matches = &app
        .version(crate_version!())
        .author("Wilfried Kopp <chevdor@gmail.com>")
        .about("Find similar lines in a file or stdin.")
        .arg(
            Arg::with_name("FILE")
                .help("Sets the input file to use. Alternatively, you may pipe <stdin>.")
                .index(1),
        )
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .arg(
            Arg::with_name("case-sensitive")
                .long("case-sensitive")
                .help("Weather the checks consider the case or not"),
        )
        .arg(
            Arg::with_name("strict")
                .long("strict")
                .help("= Max distance = 0"),
        )
        .arg(
            Arg::with_name("count")
                .long("count")
                .short("c")
                .help("Show the number of occurences"),
        )  
        .arg(
            Arg::with_name("headers")
                .long("headers")
                .short("h")
                .help("Show the headers"),
        )
        .arg(
            Arg::with_name("debug")
                .long("debug")
                .help("Debug mode, slower"),
        )
        .arg(
            Arg::with_name("distance")
                .short("d")
                .long("distance")
                .default_value("3")
                .help("The max distance"),
        )
        .arg(
            Arg::with_name("invert")
                .long("invert")
                .alias("inverted")
                .short("i")
                .help("Invert the result"),
        )
        .arg(
            Arg::with_name("full")
                .long("full")
                .short("f")
                .help("Set the score to the max"),
        )
        .arg(
            Arg::with_name("ignore-spaces")
                .long("ignore-spaces")
                .short("b")
                .help("Ignore spaces"),
        )
        .arg(
            Arg::with_name("scope")
                .short("s")
                .default_value(default_scope )
                .help("How many of the next lines are considered in the match. A larger value requires more processing."),
        )
        .arg(
            Arg::with_name("line-numbers")
                .short("l")
                .long("line-numbers")
                .alias("lnum")
                .help("How many of the next lines are considered in the match. A larger value requires more processing."),
        )
        .arg(
            Arg::with_name("algo")
                .long("algo")
                // TODO: use ScoringAlgo::get_choices below
                .possible_values(&["Levenshtein", "DamerauLevenshtein"])
                .default_value("Levenshtein")
                .help("Algo used to calculate the distance"),
        )
        .arg(
            Arg::with_name("persistence-mode")
                .long("persistence-mode")
                .possible_values(&["temp", "custom", "beside"])
                .default_value("temp")
                .help("Location where the persistence files will be stored. In 'beside' mode, the current folder will be used."),
        )
        .arg(
            Arg::with_name("persistence-folder")
                .long("persistence-folder")
                .default_value("temp")
                .required_if("persistence-mode", "custom")
                .help("Location where the persistence files will be stored if the mode is custom"),
        )
        .arg(
            Arg::with_name("no-index")
                .long("no-index")
                .takes_value(false)
                .help("Prevent saving index to disk"),
        )
        .get_matches();

    let filename = matches.value_of("FILE").map(PathBuf::from);
    let input = get_input(&filename).expect("Failure getting input");
    let line_count = input.matches("\n").count();
    let options = CliOptions::new(matches, line_count); 
    if options.presentation.verbose > 1
    {
        println!("{:#?}", options);
    }

    run(&input, filename, &options);
    Ok(())
}
